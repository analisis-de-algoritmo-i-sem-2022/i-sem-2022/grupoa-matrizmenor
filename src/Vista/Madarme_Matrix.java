/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import com.itextpdf.text.DocumentException;
import java.io.FileNotFoundException;
import java.util.Random;
import util.PDF;

/**
 *
 * @author madarme
 */
public class Madarme_Matrix {

    public static void main(String[] args) throws FileNotFoundException, DocumentException {

        String tabla_resultados = "";
        for (int i = 1;10 >= i; i++) {
            int n = (int) (Math.pow(3, i));
            byte M[][] = crear(n, n);
            //imprimir(M);
            long inicio = getTiempoEjecucion();
            String indice_cols = getMenCols(M);
            long fin = getTiempoEjecucion();
            System.out.println("------------------------- Escenario:" + i + " -----------------------------");
            System.out.println("Columna(s) menor Sumatoria:\t" + indice_cols);
            tabla_resultados += i + "\t" + n + "\t" + getMilisegundos(inicio, fin) + "\n";
        }
        System.out.println("Escenario \t Tamaño Matriz \t Tiempo Milisegundos\n");
        System.out.println(tabla_resultados);
        PDF pdf = new PDF("src/pdf/", "resultados_madarme.pdf");
        pdf.crear_Tabla(tabla_resultados, "MADARME");
    }

    private static String getMenCols(byte[][] M) {
        if (M == null) {
            throw new RuntimeException("No se puede realizar proceso");
        }
        int j = 0;
        byte sumaCols[] = new byte[M[0].length];
        while (j < M[0].length) {
            sumaCols[j] = getsumaCols(M, j);
            j++;
        }
        return getMenorColumnas(sumaCols);
    }

    private static byte getsumaCols(byte[][] M, int indice_columna) {
        byte suma = 0;
        for(int i=0;i<M.length;i++)
            suma+=M[i][indice_columna];

        return suma;
    }
    
    private static String getMenorColumnas(byte[] sumaCols) {
        byte men = sumaCols[0];
        String indice_filas = "0";
        for (int i = 1; i < sumaCols.length; i++) {
            if (sumaCols[i] < men) {
                men = sumaCols[i];
                indice_filas = i + "";
            } else if (men == sumaCols[i]) {
                indice_filas += i + ",";
            }
        }
        return indice_filas;
    }

    

    /**
     * ℕ𝕆 𝔻𝔼𝔹𝔼 𝕊𝔼ℝ ℂ𝔸𝕄𝔹𝕀𝔸𝔻𝕆
     *
     */
    private static long getTiempoEjecucion() {
        return System.nanoTime();
    }

    private static double getMilisegundos(long inicio, long fin) {
        return (fin - inicio) / 1e6;
    }

    private static byte[][] crear(int n, int m) {
        if (m <= 0 || n <= 0) {
            throw new RuntimeException("Error en el tamaño de la matriz");
        }
        Random r = new Random();
        byte M[][] = new byte[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                M[i][j] = (byte) (r.nextInt(100));
            }
        }
        return M;
    }

    private static void imprimir(byte[][] M) {
        String msg = "";
        for (byte v[] : M) {
            for (byte elemento : v) {
                msg += elemento + "\t";
            }
            msg += "\n";
        }
        System.out.println(msg);

    }
}
